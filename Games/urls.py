from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Games.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/start_game/$', 'game.views.start_game'),
    url(r'^api/player_move/$', 'game.views.player_move'),
    url(r'^api/player_stats/$', 'game.views.player_stats'),
)
