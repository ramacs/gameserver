### TESTED ENV ###

All of this was tested on a Macbook Pro running Yosemite.

### SETUP ###

1. Install MySQL and RabbitMQ.

2. mysql -u root  
Create a local database named games  
Create a local user called games_admin and give it the password as given in Games/settings.py:  
$ CREATE USER ‘games_admin'@'localhost' IDENTIFIED BY 'password’;  
GRANT ALL PRIVILEGES ON * . * TO ‘games_admin'@'localhost'; FLUSH PRIVILEGES  

3. sudo pip install -r requirements.txt

4. Run Django db migrations:
python manage.py migrate

5. Populate one game (Hangman) and two users:
python game/tools/bootstrap.py

6. Start Django server:
python manage.py runserver

7. Start celery worker:
celery -A game.tasks worker --loglevel=info

### APIs ###

1. http://localhost:8000/api/start_game/ accepts only POST requests
2. http://localhost:8000/api/player_move/ accepts only POST requests
3. http://localhost:8000/api/player_stats/ accepts only GET requests

### SAMPLE INPUTS ###

POST http://localhost:8000/api/start_game/  

Form payload:  
 
username = ram  
game = Hangman  

Output:  
{
play_token: "W1tbOCwyN10sWyJnYW1lLmNoYW5nZV9nYW1lX2luc3RhbmNlIl1dXQ:1YS1Sy:yDO3wbLyrTZxsSH6_X2m8-tS1Qs"
}
    
==================  

POST http://localhost:8000/api/player_move/  
Form payload:  

move = {"guess": "a"}  
play_token = W1tbOCwyN10sWyJnYW1lLmNoYW5nZV9nYW1lX2luc3RhbmNlIl1dXQ:1YS1Sy:yDO3wbLyrTZxsSH6_X2m8-tS1Qs
  
Output:
{
status: "InProg"
server_move: {
countLeft: 10
progressSoFar: "-a---a-"
}-
}

==================  

GET http://localhost:8000/api/player_stats/  

Output:
{
ram: {
Draw: 0,
Won: 1,
Timeout: 16,
Lost: 1,
InProg: 1
},
ram2: {
Draw: 0,
Won: 1,
Timeout: 2,
Lost: 1,
InProg: 0
}
}

### SCENARIOS THAT I TESTED ###

1. One user can play only one game at a time per spec. Requesting another game while a game is in progress throws error.
2. Game is marked timed out after time out.
3. Calling the player_move API of a completed/timed out game throws an error.
4. Hangman invalid moves (multi-character strings or characters that were already provided) throw errors.
5. Hangman valid moves make the game progress
6. Two simultaneous users and game instances didn't clobber each other.
7. Stats

### TROUBLESHOOTING HELP ###

1. RabbitMQ/Celery transport : amqp://guest:**@localhost:5672//