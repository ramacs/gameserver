from django.db import models
from  django.contrib.auth.models import User
from jsonfield import JSONField

# Create your models here.

class Game(models.Model):
    """Domain table for games. Each entry represents one game like Hangman or TicTacToe.
    """
    HANGMAN = 'Hangman'
    TIC_TAC_TOE = 'Tic-Tac-Toe'

    ALL_GAMES = (HANGMAN, TIC_TAC_TOE,)
    name = models.CharField(max_length=256, null=False)
    definition = models.TextField(null=False)
    timeout = models.PositiveIntegerField()
    # The amount of time allowed for a game (in seconds). This could vary between games. Any game exceeding this time is considered timed out.

class GameStatus:
    """Game status from the player's perspective
    """
    WON = 'Won'
    LOST = 'Lost'
    DRAW = 'Draw'
    TIMED_OUT = 'Timeout'
    IN_PROGRESS = 'InProg'

    ALL_STATUSES = (WON, LOST, DRAW, TIMED_OUT, IN_PROGRESS,)

    @classmethod
    def is_terminal_status(cls, status):
        return status != GameStatus.IN_PROGRESS

class GameInstance(models.Model):
    """An instance of a game. Each entry represents one game that is played. We are assuming one player (and the server) per game. This can be
    easily extended for a multi-player game by introducing a ParticipatingPlayer model.
    """

    game = models.ForeignKey(Game, null=False, related_name='instances')
    player = models.ForeignKey(User, null=False, related_name='game_instances')
    status = models.CharField(max_length=8, default=GameStatus.IN_PROGRESS)
    static_metadata = JSONField()
    # Any metadata that the game wants to store for an instance. This shouldn't change as the game is played. For example, in Hangman, this could
    # be the word being guessed. Store this in JSON format.
    current_board_state = JSONField()
    # The current board state in JSON format.
    play_token = models.TextField(null=False)
    last_move_number = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def get_in_progress_game_from_token(cls, play_token):
        return GameInstance.objects.get(status=GameStatus.IN_PROGRESS, play_token=play_token)

class GameMove(models.Model):
    """A move initiated by the user or server. Server moves are not being separated out into a separate table as server is
    considered just another user. This paradigm will greatly help in extending this model to multi-player games.
    """
    USER = 'User'
    SERVER = 'Server'

    ALL_PLAYERS = (USER, SERVER,)

    game_instance = models.ForeignKey(GameInstance, null=False, related_name='moves')
    move = JSONField()
    move_by = models.CharField(max_length=8, null=False)
    move_number = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
