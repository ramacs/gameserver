from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from rest_framework.serializers import ValidationError
from game.models import Game, GameInstance, GameMove, GameStatus
from access_tokens import scope, tokens
import json
from django.db import transaction
from django.http import HttpResponse
from collections import defaultdict

# Create your views here.

PERMISSION_SCOPE = "game.change_game_instance"

def get_game_definition(name):
    from game import games
    return getattr(games, name)

def create_failure_response(reason=None):
    return {'status' : 'failure', 'data' : reason}

# TODO Add authentication and authorization
@api_view(['POST'])
def start_game(request):
    """API expects a POST to start a game. Expects username and name of the game to be part of the parameters.

    If the user is currently playing a game (and it has not timed out), the user is not allowed to start another instance of the same
    or a different game (per spec, the user is allowed to have only one play token; therefore, this constraint).

    Otherwise, starts a new instance of the requested game and returns the play token.
    """
    username = request.POST['username']
    user = User.objects.get(username=username)
    if user.game_instances.filter(status=GameStatus.IN_PROGRESS).exists():
        raise ValidationError('Please complete the current game you are playing before starting a new game')

    game_name = request.POST['game']
    thisGame = Game.objects.get(name=game_name)

    static_metadata, initial_board_state = get_game_definition(thisGame.definition).create_game()

    game_instance = GameInstance.objects.create(game=thisGame, player=user, static_metadata=static_metadata, current_board_state=initial_board_state)

    play_token = tokens.generate(
        scope.access_obj(game_instance, PERMISSION_SCOPE),
    )
    game_instance.play_token = play_token
    game_instance.save()

    # Schedule a task to mark this game as timed out, if the user abandoned the game
    from game.tasks import mark_timed_out_games
    mark_timed_out_games.apply_async((game_instance.id,), countdown=thisGame.timeout)

    server_return = json.dumps({"play_token" : play_token})
    return HttpResponse(server_return, content_type="application/json")

@api_view(['POST'])
def player_move(request):
    """Authentication and authorization are somewhat built into this API as it expects the play token. As long as the play token is kept secret by the user, this serves
    as reasonable authentication and authorization.
    """
    play_token = request.POST['play_token']

    try:
        game_instance = GameInstance.get_in_progress_game_from_token(play_token)
    except:
        raise ValidationError('An in-progress game does not exist with the provided play token')

    game = game_instance.game

    is_token_valid = tokens.validate(
        play_token,
        scope.access_obj(game_instance, PERMISSION_SCOPE),
        max_age = game.timeout,
    )

    if not is_token_valid:
        raise ValidationError('The game corresponding to the token provided has expired. Please start a new game')

    move = json.loads(request.POST['move'])

    game_definition_class = get_game_definition(game.definition)
    if game_definition_class.is_legal_move(game_instance.current_board_state, move):
        with transaction.atomic():
            status, server_move, new_board_state = game_definition_class.make_move(game_instance.current_board_state, move)

            new_move_number = game_instance.last_move_number + 1
            GameMove.objects.create(game_instance=game_instance, move=move, move_by=GameMove.USER, move_number=new_move_number)
            game_instance.last_move_number = new_move_number
            game_instance.current_board_state = new_board_state
            game_instance.status = status
            game_instance.save()

            if server_move:
                new_move_number = game_instance.last_move_number + 1
                GameMove.objects.create(game_instance=game_instance, move=server_move, move_by=GameMove.SERVER, move_number=new_move_number)
                game_instance.last_move_number = new_move_number
                game_instance.save()

            server_return = {
                "status" : status,
                "server_move" : server_move or "",
            }
            return HttpResponse(json.dumps(server_return), content_type="application/json")

    else:
        raise ValidationError('Invalid move')

# TODO Add authentication and authorization. Maybe you want your stats to be exposed only to you or to users in the same groups as you.
# Pagination may also be required as we scale.
@api_view(['GET'])
def player_stats(request):
    server_return = defaultdict(dict)

    stats = {}
    for status in GameStatus.ALL_STATUSES:
        stats[status] = defaultdict(int)

    for gameInstance in GameInstance.objects.all():
        stats[gameInstance.status][gameInstance.player.id] += 1

    for user in User.objects.all():
        for status in GameStatus.ALL_STATUSES:
            server_return[user.username][status] = stats[status][user.id]

    return HttpResponse(json.dumps(server_return), content_type="application/json")

