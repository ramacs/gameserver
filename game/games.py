from game.models import GameStatus


class Game:
    """Any game has to implement these abstract methods
    """
    @classmethod
    def create_game(cls):
        raise Exception("Method not implemented")

    @classmethod
    def is_legal_move(cls, current_board_state, move):
        raise Exception("Method not implemented")

    @classmethod
    def make_move(cls, current_board_state, move):
        raise Exception("Method not implemented")

class Hangman(Game):
    """A very simple version of the Hangman game. The server is always going to ask the player to guess the GUESS_WORD. MAX_TRIES
    guesses are allowed by the user. If the word is not completed by MAX_TRIES guesses, the player loses. If it is completed by MAX_TRIES
    guesses, the player wins. DRAW is not a possibility in this game.
    """
    GUESS_WORD = "Hangman".lower()
    MAX_TRIES = 10

    TARGET = 'target'
    HITS = 'hits'
    COUNT_LEFT = 'countLeft'
    GUESS = 'guess'
    PROGRESS_SO_FAR = 'progressSoFar'

    @classmethod
    def create_game(cls):
        static_metadata = {Hangman.TARGET : Hangman.GUESS_WORD}
        initial_board_state = {Hangman.HITS : [], Hangman.COUNT_LEFT : Hangman.MAX_TRIES}
        return (static_metadata, initial_board_state)

    @classmethod
    def is_legal_move(cls, current_board_state, move):
        # The move is legal only if it is a character that has not been presented already
        guess = move[Hangman.GUESS].lower()
        return len(guess) == 1 and (not guess in current_board_state[Hangman.HITS])

    @classmethod
    def make_move(cls, current_board_state, move):
        # return status, server_move, new_board_state
        moveChar = move[Hangman.GUESS].lower()
        new_hits = current_board_state[Hangman.HITS] + [moveChar]
        count_left = current_board_state[Hangman.COUNT_LEFT]
        if moveChar not in Hangman.GUESS_WORD:
            count_left -= 1

        done = True
        progress_so_far = ""
        for c in Hangman.GUESS_WORD:
            if c not in new_hits:
                done = False
                progress_so_far += "-"
            else:
                progress_so_far += c

        if done:
            status = GameStatus.WON
        else:
            status = GameStatus.IN_PROGRESS
            if count_left <= 0:
                status = GameStatus.LOST

        new_board_state = {Hangman.HITS : new_hits, Hangman.COUNT_LEFT : count_left}
        return (status, {Hangman.COUNT_LEFT : count_left, Hangman.PROGRESS_SO_FAR : progress_so_far}, new_board_state)
