import os, sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'Games.settings'
import django
django.setup()

from game.models import Game
from  django.contrib.auth.models import User

# Data for the Game domain table
Game.objects.create(name='Hangman', definition='Hangman', timeout=300)

# Some test users
User.objects.create_user('ram', 'ramEmail@example.com', 'ramPassword')
User.objects.create_user('ram2', 'ramEmail2@example.com', 'ramPassword2')