import os, sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'Games.settings'
import django
django.setup()

from models import GameInstance, GameStatus

from celery import Celery
app = Celery('game.tasks', broker='amqp://guest@localhost//')

@app.task
def mark_timed_out_games(game_instance_id):
    game = GameInstance.objects.get(id=game_instance_id)
    if not GameStatus.is_terminal_status(game.status):
        game.status = GameStatus.TIMED_OUT
        game.save()
